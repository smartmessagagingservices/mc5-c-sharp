﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace intellisms_mc5_rest
{
    class SendSingle2Response
    {
        public const int MESSAGE_STATUS_SUBMITTED = 10;

        public int messageStatus { get; set; }
        public String documentId { get; set; }
        public String recipient { get; set; }

        public bool HasError { get { return MESSAGE_STATUS_SUBMITTED.Equals(messageStatus); } }

        public override string ToString()
        {
            return String.Format("messageStatus:{0} docuementId:{1} recipient:{2}", messageStatus, documentId, recipient);
        }
    }
}
