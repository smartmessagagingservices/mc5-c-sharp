﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace intellisms_mc5_rest
{
    class Mc5ClientConfig
    {
        public const String DEFAULT_SERVICE_URL = @"http://mc5.smartmessagingservices.net/services/rest/"; // Don't forget to add the trailing backslash 

        public String ServiceUrl { get; private set; }
        public String Username { get; private set; }
        public String Password { get; private set; }

        public Mc5ClientConfig(String username, String password) : this(DEFAULT_SERVICE_URL, username, password) { }

        public Mc5ClientConfig(String serviceUrl, String username, String password)
        {
            ServiceUrl = serviceUrl;
            Username = username;
            Password = password;
        }
    }
}
