﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace intellisms_mc5_rest
{
    class BalanceResponse
    {
        public int status { get; set; }
        public double credits { get; set; }

        public bool HasError { get { return 1.Equals(status); } }

        public override string ToString()
        {
            return String.Format("status: {0} credits: {1}", status, credits);
        }
    }
}
