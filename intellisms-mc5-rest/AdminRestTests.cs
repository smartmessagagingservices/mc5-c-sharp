﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.Net.Http;
using System.Collections.Generic;
using System.Net;

namespace intellisms_mc5_rest
{
    [TestClass]
    public class AdminRestTests
    {
        private readonly Mc5ClientConfig _config = new Mc5ClientConfig(
            "<insert your MC5 reseller username>",
            "<insert your MC5 reseller password>");

        private const String TEST_CUSTOMER = "<insert customer name>"; 

        [TestMethod]
        public void QueryBalanceWebApi()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(_config.ServiceUrl);
            HttpContent httpContent = new FormUrlEncodedContent( new[]
            {
                new KeyValuePair<string, string>("reseller", _config.Username),
                new KeyValuePair<string, string>("password", _config.Password),
                new KeyValuePair<string, string>("customer", TEST_CUSTOMER)
            });

            HttpResponseMessage response = client.PostAsync("admin/balance", httpContent).Result;
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode, "The request did not return OK");

            var result = response.Content.ReadAsAsync<BalanceResponse>().Result;
            Assert.AreNotEqual(1, result.HasError, "The response returned an error");

            Trace.WriteLine(result.ToString());
        }

    }
}
