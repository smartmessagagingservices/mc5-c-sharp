﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.Net.Http;
using System.Collections.Generic;
using System.Net;

namespace intellisms_mc5_rest
{
    [TestClass]
    public class SendSingle2Tests
    {
        private readonly Mc5ClientConfig _config = new Mc5ClientConfig(
            "<insert your MC5 username>",
            "<insert your MC5 password>");

        private const String TEST_RECIPIENT = "<insert recipient number>";

        [TestMethod]
        public void SendSimpleTextMessage()
        {
            HttpContent httpContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("username", _config.Username),
                new KeyValuePair<string, string>("password", _config.Password),
                new KeyValuePair<string, string>("recipient", TEST_RECIPIENT),
                new KeyValuePair<string, string>("text", @"Test simple send single message")
            });

            SendSingle2Response result = SendSingle2Actual(httpContent);
            Trace.WriteLine(result.ToString());
        }
        //In the above example the system will set your source address to a number from our rotation pool and reply message value will be set to the default of "log"

        public void SendSimpleTextMessage1()
        {
            HttpContent httpContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("username", _config.Username),
                new KeyValuePair<string, string>("password", _config.Password),
                new KeyValuePair<string, string>("recipient", TEST_RECIPIENT),
                new KeyValuePair<string, string>("text", @"Test simple send single message"),
                new KeyValuePair<string, string>("replyTo", "mailto:johndoe@mydomain.com")
            });
            SendSingle2Response result = SendSingle2Actual(httpContent);
            Trace.WriteLine(result.ToString());
        }
        // in the above example reply messages will be forwarded to the email address defined in the replyTo parameter
        public void SendSimpleTextMessage2()
        {
            HttpContent httpContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("username", _config.Username),
                new KeyValuePair<string, string>("password", _config.Password),
                new KeyValuePair<string, string>("recipient", TEST_RECIPIENT),
                new KeyValuePair<string, string>("text", @"Test simple send single message"),
                new KeyValuePair<string, string>("replyTo", "http://requestb.in/vw66muvw?from%3D%7B%7Bfrom%7D%7D%26text%3D%7B%7Btext%7D%7D")
            });
            SendSingle2Response result = SendSingle2Actual(httpContent);
            Trace.WriteLine(result.ToString());
        }
        // in the above example reply messages will be forwarded webservice listening for GET requests  decoded url is http://requestb.in/vw66muvw?from={{from}}&text={{text}} 

        public void SendSimpleTextMessage3()
        {
            HttpContent httpContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("username", _config.Username),
                new KeyValuePair<string, string>("password", _config.Password),
                new KeyValuePair<string, string>("recipient", TEST_RECIPIENT),
                new KeyValuePair<string, string>("text", @"Test simple send single message"),
                new KeyValuePair<string, string>("sender", "Peter")
            });
            SendSingle2Response result = SendSingle2Actual(httpContent);
            Trace.WriteLine(result.ToString());
        }
        //in the above message the sender is being set to a name - no replyTo value is set;


        [TestMethod]
        public void SendUnicodeMessage()
        {
            HttpContent httpContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("username", _config.Username),
                new KeyValuePair<string, string>("password", _config.Password),
                new KeyValuePair<string, string>("recipient", TEST_RECIPIENT),
                new KeyValuePair<string, string>("encoding", "ucs2"),
                new KeyValuePair<string, string>("text", @"新年快乐")
            });
            
            // The encoding of the actual content payload needs to be set to UTF-8
            httpContent.Headers.ContentType.CharSet = "UTF-8";

            SendSingle2Response result = SendSingle2Actual(httpContent);
            Trace.WriteLine(result.ToString());
        }

        private SendSingle2Response SendSingle2Actual(HttpContent httpContent)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(_config.ServiceUrl);
            HttpResponseMessage response = client.PostAsync("message/sendSingle2", httpContent).Result;
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode, "The request did not return OK");

            var result = response.Content.ReadAsAsync<SendSingle2Response>().Result;
            Assert.AreNotEqual(SendSingle2Response.MESSAGE_STATUS_SUBMITTED, result.HasError, "The response returned an error");
            Assert.IsNotNull(result.documentId, "documentId not in response");
            Assert.AreEqual(TEST_RECIPIENT, result.recipient, "recipient submitted was not returned in response");
            return result;
        }
    }
}
